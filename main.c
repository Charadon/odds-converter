/* 
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "stdio.h"
#include "stdlib.h"

int choice;
double number;

void OddsToPercentage(){
	printf("Enter odds: 1 in ");
	scanf("%lf", &number);
	number = 1 / number;
	number = number * 100;
	printf("\nThe percentage is: %lf\%\n", number);
	exit(0);
}

void PercentageToOdds(){
	printf("Enter percentage: ");
	scanf("%lf", &number);
	number = number / 100;
	number = 1 / number;
	printf("\nThe odds are: 1 in %lf\n", number);
	exit(0);
}

int main(){
	printf("This program converts odds to percentages, and vice versa.\nThis program is licensed under the GPL3.0\n");
	printf("Select Conversion:\n(1) Percentage to Odds\n(2) Odds to Percentage\n");
	choice:
	printf("Choice: ");
	scanf("%i", &choice);
	switch(choice){
		case 1:
			PercentageToOdds();
			break;
		case 2:
			OddsToPercentage();
			break;
		default:
			printf("Invalid Answer.\n");
			goto choice; //Redo's Choice
			break;
	}
}
