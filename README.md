<b>How to compile</b>
<br />
Simply run this command: <i>gcc main.c -o converter</i>
<hr />
This program converts odds to percentages, and vice versa. This is a sibling project to my odds-roller program (https://gitlab.com/Charadon/odds-roller), though it can be useful on its own.